import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewCardsComponent } from './new-cards/new-cards.component';



@NgModule({
  declarations: [
    NewCardsComponent
  ],
  exports: [
    NewCardsComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
