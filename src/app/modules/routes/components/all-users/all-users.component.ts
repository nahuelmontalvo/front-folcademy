import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../services/users/users.service';

@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.scss'],
  providers: [UsersService]
})
export class AllUsersComponent implements OnInit {

  users: any;

  constructor(
    private userService: UsersService
  ) {
    this.getUsers();
  }

  ngOnInit(): void {
  }

  getUsers() {
    this.userService.getUsers().subscribe(
      response => {
        this.users = response.data;
        console.log(this.users);
      }, error => {
        console.log(error);
      }
    )
  }

}
